module SimplePrefixTree where

import           Data.IntMap (IntMap)
import qualified Data.IntMap as Map
import           Data.List (foldl', foldl1')
import           Data.Maybe

data PreTree = Node Bool (IntMap PreTree)
  deriving (Show)

fromList :: [[Int]] -> PreTree
fromList = foldl' (flip insert) empty

empty :: PreTree
empty = Node False Map.empty

singleton :: [Int] -> PreTree
singleton = (`insert` empty)

match1 :: PreTree -> Int -> Maybe PreTree
match1 t = match t . pure

match :: PreTree -> [Int] -> Maybe PreTree
match t [] = Just t
match (Node _ t) (x:xs) = Map.lookup x t >>= flip match xs

insert :: [Int] -> PreTree -> PreTree
insert [] (Node _ c) = Node True c
insert (x:xs) (Node p c) = Node p (Map.alter ins x c)
  where
    ins Nothing = Just (singleton xs)
    ins (Just t) = Just (insert xs t)

isPrefix :: [Int] -> PreTree -> Bool
isPrefix xs = isJust . flip match xs

intersection :: PreTree -> PreTree -> PreTree
intersection (Node a x) (Node b y) =
  Node (a && b) (Map.intersectionWith intersection x y)

intersections :: [PreTree] -> PreTree
intersections = foldr1 intersection

children :: PreTree -> [(Int, PreTree)]
children (Node _ c) = Map.assocs c

elems :: PreTree -> [[Int]]
elems t = map reverse $ elems' [] t
  where
    elems' pre (Node pres c)
      | pres = pre : xs
      | otherwise = xs
      where xs = concat [ elems' (x:pre) tree | (x, tree) <- Map.assocs c]

b = fromList [[1,2,3], [3,2,1], [1,3,2]]
a = fromList [[1,2,3], [1,2], [1,2,3,4], [1,3,2]]
c = intersection a b

module MagicSquare
    ( defaultMain
    , isMagicSquare
    , findMagicSquares
    ) where

import           Data.IntSet (IntSet)
import qualified Data.IntSet as Set
import           Data.List
import           Data.Maybe
import           SimplePrefixTree
import           Tuple

t = defaultMain 4 34 [1..16]

bitsK :: Int -> Int -> [[Bool]]
bitsK bits ones
  | bits == ones = [replicate bits True]
  | ones == 0 = [replicate bits False]
  | otherwise = map (True :) (bitsK (bits - 1) (ones - 1))
    ++ map (False :) (bitsK (bits - 1) ones)

subsetsK :: Int -> [Int] -> [[Int]]
subsetsK k xs = map (mapMaybe keep . zip xs) bits
  where
    bits = bitsK (length xs) k
    keep (_, False) = Nothing
    keep (x, True) = Just x

subsetSum :: Int -> Int -> [Int] -> [[Int]]
subsetSum s k = filter ((==s) . sum ) . subsetsK k

defaultMain :: Int -> Int -> [Int] -> IO ()
defaultMain side magicNum = mapM_ putStr . zipWith withNum [1..] . map showSquare . findMagicSquares side magicNum
  where
    withNum n s = unlines ["[" ++ show n ++ "]", s]

showSquare :: [[Int]] -> String
showSquare = unlines . map (unwords . map show)

findMagicSquares :: Int -> Int -> [Int] -> [[[Int]]]
findMagicSquares side magicNum xs = go [] [] [] (0,0) Set.empty
  where
    possibleRows = concatMap permutations $ subsetSum magicNum side xs
    baseTree = fromList possibleRows
    go :: [[Int]] -> [(Int, Tuple4 (Maybe PreTree))]
       -> [(Int, Tuple4 (Maybe PreTree))] -> (Int, Int) -> IntSet -> [[[Int]]]
    go top prev curr (i, j) used
      | i == side = [top ++ [map fst prev]]
      | j == side =
        let newRow
              | i == 0 = []
              | otherwise = [map fst prev]
        in go (top ++ newRow) curr [] (i + 1, 0) used
      | otherwise =
        concat [ go top prev (curr ++ [(x, map4 (matchx. fromMaybe baseTree) prevTrees)])
                 (i, j + 1) (Set.insert x used)
               | (x, _) <- children treeAtij, x `Set.notMember` used, let matchx = flip match1 x ]
      where
        treeAtij
          | null $ catMaybes $ cat4 prevTrees = baseTree
          | otherwise = intersections (catMaybes $ cat4 prevTrees)
        prevTrees :: Tuple4 (Maybe PreTree)
        prevTrees = (left, up, leftUp, rightUp)
        left
          | j == 0 = Nothing
          | otherwise = sel4 1 $ snd $ last curr
        up
          | i == 0 = Nothing
          | otherwise = sel4 2 $ snd $ prev !! j
        leftUp -- diagonal
          | i == j && i > 0 = sel4 3 $ snd $ prev !! (j - 1)
          | otherwise = Nothing
        rightUp -- diagonal
          | i + j == side - 1 && i > 0 = sel4 4 $ snd $ prev !! (j + 1)
          | otherwise = Nothing



-- | Checks whether it is a magical square
isMagicSquare :: [[Int]] -> Bool
isMagicSquare rows = isSquare && magicRows && magicCols && magicDiags
  where
    side = length rows
    isSquare = all ((==side) . length) rows
    cols = transpose rows
    diag1 = [rows!!i!!i | i <- [0..side - 1]]
    diag2 = [rows!!i!!i | i <- [0..side - 1], let j = side - i - 1]
    diags = [diag1, diag2]
    magicNum = sum (head rows)
    magic = (==magicNum) . sum
    magicRows = all magic rows
    magicCols = all magic cols
    magicDiags = all magic diags

module Tuple where

type Tuple4 a = (a,a,a,a)

sel4 :: Int -> Tuple4 t -> t
sel4 1 (a, _, _, _) = a
sel4 2 (_, a, _, _) = a
sel4 3 (_, _, a, _) = a
sel4 4 (_, _, _, a) = a
sel4 _ _ = error "tuple invalid index"

cat4 :: Tuple4 a -> [a]
cat4 (a, b, c, d) = [a,b,c,d]

map4 :: (t -> t1) -> (t, t, t, t) -> (t1, t1, t1, t1)
map4 f (a, b, c, d) = (f a, f b, f c, f d)

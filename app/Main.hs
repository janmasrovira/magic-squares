module Main where

import MagicSquare
import System.Environment

main :: IO ()
main = do
  (s:m:xs') <- getArgs
  let side = read s
      magicNum = read m
      xs
        | null xs' = [1..side*side]
        | otherwise = map read xs'
  putStrLn $ "side = " ++ s
  putStrLn $ "magic number = " ++ m
  putStrLn $ "elements = " ++ unwords (map show xs)
  defaultMain side magicNum xs

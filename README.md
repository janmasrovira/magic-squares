# README #

An algorithm for generating all magic squares of a given size, using a given magic number and a given set of integers.
See https://en.wikipedia.org/wiki/Magic_square

In output/squares-4x4-34.txt.xz there are all the squares of side 4 and magical number 34 (each row, column and diagonal sums 34). Total of 7040 squares.
It took 45 seconds to compute.